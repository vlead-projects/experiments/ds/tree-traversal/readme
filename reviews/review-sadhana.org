#+TITLE: Review of Tree Traversal Experiment
#+AUTHOR: VLEAD
#+DATE: [2019-05-02 Thu]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
  This document captures the code reviews comments of tree
  traversal experiment.

* Review Comments
  Please find the overall review comments below:
** Documentation
   1. Readme repository doesn't have the necessary documents
      The same is created as an issue [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/readme/issues/1][here]]. Please include
      all the documents and update the issue.

   2. Create an "Index" document listed down with all the
      artefacts. The same is created as an issue
      [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/1][here]]. Include the document and update the issue.

** Code 
   1. Indentation of the code is not followed in many
      places. Please correct it.

   2. Literate for the code is not written
      properly. Elaborate and write the description for each
      code block.

   3. Used comments inside the code blocks to describe
      it. Please remove comments from the code block and add
      them as description to the code block.

   4. Remove commented code from the content,
      artefacts(HTML, JS), etc.,

   5. Remove inline JS from the HTML aretfacts.
     
   6. The entire styles are placed in one entire code block
      in org. Styles for each DOM element must be placed as
      a single block with a proper description i.e., for
      which DOM element this style is been applied.

   7. Duplicate styles with two different file naming
      conventions( [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/blob/test/src/runtime/css/dftdemostyle.org][dftdemostyle.org]], [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/blob/test/src/runtime/css/style.org][style.org]] ) exits in
      the artefacts repo. 

   8. There is a lot of commented code in the style
      sheets. Remove it to make the code look
      cleaner. Example: [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/blob/test/src/runtime/css/hint_box.org][hint_box.org]]

   9. Use shorthand properties for styling.

   10. Repetition of same styles in style sheets. Create a
       common style sheet with these styles so that, it can
       used in artefacts wherever it's needed.

   11. There is a lot of commented JavaScript code which is
       which is making the code difficult to understand.

   12. Too many functions are wrapped inside one code block
       which is making difficult to understand. Please
       separate it or add clear description which explains
       about each and every method in that block.

   13. There is a common code which is used in all the JS
       artefacts. Move this to a common library to make the
       code modular.

   14. Each code block in org should be given an unique
       name.

** Functionality
   1. Interactive artefacts are not responsive The same is
      created as an issue [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/2][here]]. Please fix and update the
      issue.
      
   2. Issues in the "Breadth First Traversal Demo" are
      recorded as an issue [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/3][here]]. Please fix the issue and
      update here.
  
   3. Issues in the "Breadth First Traversal Practice and
      Exercise" are recorded as an [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/4][issue]]. Please fix the
      issues and update it.

   4. Issues in the "Depth First Traversal Demo" are
      recorded as an issue [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/5][here]]. Please fix the issue and
      update here.
   
   5. Issues in the "Depth First Traversal Practice": are
      recorded as an issue [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts/issues/6][here]]. Please fix the issue and
      update here.
