#+TITLE: DESIGN SPEC FOR TREE TRAVERSAL
#+AUTHOR: VLEAD
#+DATE: [2019-05-8 Thu]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: \n:t

This is the desgin document for Tree Traversal experiment includes :
+ Mapping of experiment level learning objectives to Learning Units
+ Mapping of LU level learning objectives to tasks
+ List of artefacts under each task
+ Design specifications for each artefact
+ Acceptance testing for artefacts

* LEARNING OBJECTIVES AT EXPERIMENT LEVEL :

| LEARNING OBJECTIVE | LEARNING UNIT | BLOOM's TAXONOMY LEVEL |
|--------------------+--------------+----------|
|To recall notion of trees| PRE-TEST              | Remember |
|To describe traversal and search                        | Traversal          | Remember and Understand |
|To discuss variations of depth first traversal                   | Depth First Traversal | Analyze|
|To discuss breadth first traversal  | Breadth First Traversal             | Analyze |
|To apply the concepts learnt and test understanding of user| POST-TEST             | Evaluate |


* LEARNING UNITS AND LIST OF TASKS UNDER EACH LEARNING UNIT

** LU 1 : Pre-test 
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
| Recap    | To review pre requisites | Full |
| Pre-test | To test user's understanding of pre requisites | Full |

** LU 2 : TreeTraversal
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
|Intro : Intuition | To show difference of traversal and search | Full |

** LU 3 : Depth First Traversal
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
|Iterative | To introduce to the iterative algorithm | Full|
|Recursive | To introduce to recursive algorithm | Full |
|Demo | To show visualisations of inorder,preorder,postoder | Full |
|Practice | To give step by step feedback to user about each step the user selects| Full |
|Exercise | To let the user apply his learnings and check if is correct at the end | Full |
|Quiz | To test conceptual understanding | Full |


** LU 4 : Reconstructing Binary Tree
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
|From Inorder and Postorder | To understand the applications of traversal | Full |
|From Inorder and Preorder | To understand the applications of traversal | Full |
|Quiz | To test conceptual understanding | Full |


** LU 5 : Breadth First Traversal
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
|BFT Theory | To introduce to the the Breadth first algorithm | Full|

| BFT Demo | To show visualisations of breadth first traversal | Full |
| Breadth First Practice | To give step by step feedback to user about each step the user selects| Full |
| Breadth First Exercise | To let the user apply his learnings and check if is correct at the end | Full |
|Quiz | To test conceptual understanding | Full |



** LU 6 : Post Test
| TASK | LU LEARNING OBJECTIVE | FULFILMENT LEVEL |
|------+----------+----|
| Post-test | To test user's understanding of tree traversal | Full |

* LIST OF ARTEFACTS IN EACH TASK

** LU 1 : Pre-test

| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|


|Pre-test | Quiz questions | Each question should test understanding of tree data structure | 

** LU 2 :  Tree Traversals


| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|
|Preamble | Video | Video Walkthroughs the entire experiment |
|Concept | Text | Text should introduce concept of tree traversal |
|Concept | Text | Text should elaborate on difference between depth first traversal and breadth first traversal |
|Concept| Text | Text should describe the difference between search and traversal |




** LU 3 : Depth First Traversal


| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|
|Preamble | Video | Video should introduce the concepts of search and traversal and describe each type of depth first traversal in detail |
|Concept | Text | Text should describe in detail how depth first traversal works|
|Concept | Image | Image should give the user a mind map of how the depth first traversal algorithm works|



|Concept | Image | Image should give the user a mind map of how the inorder depth first traversal algorithm works|
|Concept | Image | Image should give the user a mind map of how the preorder depth first traversal algorithm works|
|Concept | Image | Image should give the user a mind map of how the postorder depth first traversal algorithm works|


|Demo | Interactive Element | IE will provide a randomly generated tree . User can click on INORDER,PREORDER,POSTORDER  to start the demo.After the corresponding demo is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed. |
|Practice | Interactive Element | User clicks Generate Graph to generate a random graph.User can click on INORDER,PREORDER,POSTORDER  to start the practice artefact.The user can click the node as per the traversal algorithm chosen.User is given feedback on whether he has chosen the right step or not,and he can undo his steps if he has gone wrong at some point.After the corresponding practice  is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed. |

|Exercise | Interactive Element | User clicks Generate Graph to generate a random graph.User can click on INORDER,PREORDER,POSTORDER  to start the exercise artefact.The user can click the node as per the traversal algorithm chosen.User is given feedback after he has completed the entire traversal and whether he has done it right or wrong.After the corresponding exercise  is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed.|




** LU 4 : Reconstructing Binary Tree


| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|
|Concept | Image | Image should show the basic algorithm to reconstruct a tree from given inorder and postorder traversal |

|Concept | Image | Image should show the basic algorithm to reconstruct a tree from given inorder and preorder traversal |



** LU 5 : Breadth First Traversal


| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|
|Preamble | Video | Video should introduce the concepts of search and traversal and describe breadth first traversal in detail |


|Concept | Text | Text should describe in detail how breadth first traversal works|
|Concept | Image | Image should give the user a mind map of how the breadth first traversal algorithm works|



|Concept | Image | Image should give the user a mind map of how the breadth first traversal algorithm works|
|Concept | Image | Image should contrast the difference between depth first and breadth first traversals|


|Demo | Interactive Element | IE will provide a randomly generated tree . User can click on BREADTH FIRST  to start the demo.After the corresponding demo is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed. |
|Practice | Interactive Element | User clicks Generate Graph to generate a random graph.User can click on  FIRST start the practice artefact.The user can click the node as per the traversal algorithm chosen.User is given feedback on whether he has chosen the right step or not,and he can undo his steps if he has gone wrong at some point.After the corresponding practice  is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed. |

|Exercise | Interactive Element | User clicks Generate Graph to generate a random graph.User can click on BREADTH FIRST  to start the exercise artefact.The user can click the node as per the traversal algorithm chosen.User is given feedback after he has completed the entire traversal and whether he has done it right or wrong.After the corresponding exercise  is complete,user need to click RESET to get the tree again and proceed to the next traversal to be viewed.|








** LU 5 : Post-test 

| TASK | ARTEFACT TYPE | DESIGN SPEC |
|------+----------+-------------|
|Pre-test | Quiz questions | Each question should test understanding of tree traversals  | 

* Acceptance Testing

** Acceptance Testing for Video Artefacts

+ Given video, when I load page, video loads 
+ Given video, when I press play button, video plays
+ Given video, when I press pause button, video stops

** Acceptance testing for Interactive Elements

*** Demo artefacts

+ Given artefact, when I click on corresponding traversal, demonstration starts

+ Given artefact, when I click on reset, fresh tree is loaded


*** Practice artefacts

+ Given artefact, when I click on start, practice starts
+ Given artefact, when I click on each node,the comment box shows if i chose the right step.
+ Given artefact, when I click on each node,the comment box shows the hint for the next step to be chosen.
+ Given artefact, when I click on traversal i cannot select generate graph while doing the practice


*** Exercise artefacts

+ Given artefact, when I click on start, practice starts
+ Given artefact, when I click on each node and complete the traversal,the comment box shows if i chose the right traversal or not.
+ Given artefact, when I click on each node,the comment box shows the hint for the next step to be chosen.
+ Given artefact, when I click on traversal i cannot select generate graph while doing the practice


* MAPPING TABLE 3 :

| LEARNING TASK | Gagne's Instruction |
|--------------------+--------------+----------|
|To recall notion of traversal and trees|  3-Simulate recall of prior learning |
|To describe depth first and breadth first algorithms                       | 4-Present the content |
|To discuss search and traversal  | 4-Present the content|
|To illustrate time and space complexity of the algorithm         | 4-Present the content |
|To apply the concepts learnt and test understanding of user    | 6-Elicit Performance and 8-Assess performance |

* MAPPING TABLE 4 :

| LEARNING TASK | Merrill's Principle Involved |
|--------------------+--------------+----------|
|To recall notion of traversal and trees|  Problem (Task-centered) |
|To describe depth first and breadth first algorithms                        | Problem (Task-centered) |
|To discuss search and traversal      | Problem (Task-centered)|
|To illustrate time and space complexity of the algorithm         | Problem (Task-centered) |
|To apply the concepts learnt and test understanding of user    | Activation, Demonstration and Application |

