#+TITLE: TREE TRAVERSAL EXPERIMENT DASHBOARD
#+AUTHOR: VLEAD
#+DATE: [2019-05-8 Thu]
#+SETUPFILE: org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil'

Tree Traversal : Spring 2019 offcycle internship

* Introduction
  This experiment will introduce the learner to the basics
  of tree traversal.  The experiment covers topics related
  to depth first and breadth first traversal.

* Hosted Link
  Hosted Link for the Selection Sort experiment :: [[http://exp-iiith.vlabs.ac.in/tt/exp.html][Tree Traversal Link]]

* Scope of the project
  This experiment will enable students to visulize tree as a
  data structure and understand how it can be traversed in
  different ways according to the need.

* Project Developers and Contributors Details
  |------------------+-----------------------+----------------------------------|
  | *Names*          | Suchismith Roy          | Neeraj Barthwal	            |
  |------------------+-----------------------+----------------------------------|
  | *Year of Study*  | 1st year Mtech IIIT-H | 1st year Mtech IIIT-H         |
  |------------------+-----------------------+----------------------------------|
  | *Phone-No*       | 9620405185                 | 7899764047                       |
  |------------------+-----------------------+----------------------------------|
  | *Email-ID*       | suchismith.roy@students.iiit.ac.in  | neeraj.barthwal@students.iiit.ac.in         |
  |------------------+-----------------------+----------------------------------|
  | *gitlab handles* | suchismith1993            | neeraj.barthwal                  |
  |------------------+-----------------------+----------------------------------|

* References
  A mapping between different documents and their roles.
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  | *S.NO* | *Link*                  | *Role*                                                                                                                                                                   |
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
  |     1. | [[./structure.org][Structure]]               | Captures the structure of experiment content                                                                                                                             |
  |     2. | [[./design-spec.org][Design Spec]]             | Captures the design specs of the experiment structure and artefacts                                                                                                      |
  |     3. | [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/content-html][Content]]                 | Captures the experiment content                                                                                                                                          |
  |     4. | [[https://gitlab.com/vlead-projects/experiments/ds/tree-traversal/artefacts][Artefacts]]               | This is where the artefacts of the experiments are implemented                                                                                                           |
  |     5. | [[https://docs.google.com/document/d/1DdRnoZguDg4jJkOk-D5MUDr7mCATExBVw6QSi1XioG0/edit][Deployment-Instructions]] | This document describes (build process and changes) reducing the manual work in each experiment deployment process and changes which we have made to reduce manual work. |
  |--------+-------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------|

* External pictures/videos used : None
* Number of artefacts : 6
+ 2 demo artefacts
+ 2 practice artefacts
+ 2 exercise artefacts

* Assumptions
 /User's knowledge assumptions/ 
 - Basics of Tree
 - Time Complexity, Arrays
   
* StakeHolders
 - College and School Students
 - MHRD
 - Teachers

* Value Added by our project
 - It would be beneficial for engineering students as Tree
   traversals has huge applications in several algorithms
   and is a base to understand tougher datastructures.
 - Highly beneficial for tier 2 and tier 3 college students
   who can use this to learn and understand the concept of
   tree traversals.
   
* Risks and Challenges
 - One challenge was to make artefacts in such a way that
   they can be rendered properly by the existing
   infrastructure in a proper manner which was quite rigid.
 - Difficult to make the entire area in a node clickable.
 - Making the user undo and redo every step was the toughest
   part.
 
* Learnings
 - Literate programming
 - Better understanding of d3 library and javascript
   fundamentals

 

